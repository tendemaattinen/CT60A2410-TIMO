package harkkatyö;


import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.image.Image;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

/** 
 * Author: Teemu Tynkkynen
 * Student number: 0418815
 * Date: 16.12.2015
 * 
 * FXML controller for starting window
 */

public class FXMLDocumentController implements Initializable {
    
    @FXML
    private WebView webView;
    @FXML
    private Button addToMap;
    @FXML
    private ComboBox<String> automatonLocations;
    @FXML
    private Button create;
    @FXML
    private ComboBox<String> storage;
    @FXML
    private Button refresh;
    @FXML
    private Button sentPackage;
    @FXML
    private Button removePaths;
    @FXML
    private Button openLog;

    private WebEngine engine;
    Main main  = Main.getInstance();
    
    /**
     * Initialize the controller class
     * 
     * @param url
     * @param rb 
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {   
        engine = webView.getEngine();
        engine.load(getClass().getResource("res/index.html").toExternalForm());
        
        for (int i = 0; i < main.getLenght(); i++) {
            String city = main.getCity(i);
            if (i == 0) {
                automatonLocations.getItems().addAll(city);
            } else {
                if (city.equals(main.getCity(i-1))) {
                } else {
                    automatonLocations.getItems().addAll(city);
                }
            }
        } 
    }    

    /** 
     * Adds marker to map
     * 
     * @param event
     * @throws IOException 
     */
    @FXML
    private void handleAddToMapAction(ActionEvent event) throws IOException {
        // Gives error when user hasn't choose any city
        if (automatonLocations.getValue() == null) {
            main.setError("Anna kaupunki");
            main.createErrorWindow();
        } else {
            String locat = automatonLocations.getValue();
            int j = 0;
            for (int i = 0; i < main.getLenght(); i++) {
                String a = main.getCity(i);
                if(locat.equals(a)) {
                    j = i;
                    String xx = main.addToMap(i);
                    engine.executeScript("document.goToLocation(" + xx + ",'orange')");
                }
            }
            main.addToLog(main.getTime() + " Kaupungin " + main.getCity(j) + " automaatit lisätty kartalle"); 
        }       
    }
    
    /** 
     * Opens FXMLNewPackage window where user can make new package
     * 
     * @param event
     * @throws IOException 
     */
    @FXML
    private void handleCreateAction(ActionEvent event) throws IOException {
        Stage stage = new Stage();
        Parent page = FXMLLoader.load(getClass().getResource("FXMLNewPackage.fxml"));
        Scene scene = new Scene(page);
        stage.setScene(scene);
        stage.getIcons().add(new Image(getClass().getResourceAsStream("res/T-logo.png")));
        stage.setTitle("Paketin luonti");
        stage.show();  
    }
    
    /**
     * Refresh storage
     * 
     * @param event 
     */
    @FXML
    private void handleRefreshAction(ActionEvent event) {
        storage.getItems().clear();
        for (int i = 0; i < main.getSize(); i++){
            storage.getItems().addAll(main.getId(i) + "\t" + main.getName(i));
        }
    }

    /** 
     * Sents package
     * 
     * @param event
     * @throws IOException
     * @throws InterruptedException 
     */
    @FXML
    private void handleSentPackageAction(ActionEvent event) throws IOException, InterruptedException {
        // Gives error when user hasn't choose package
        if (storage.getValue() == null){
            main.setError("Valitse lähetettävä paketti");
            main.createErrorWindow();
        } else {
            int b = Integer.parseInt(storage.getValue().substring(0,1));
            for (int i = 0; i < main.getSize(); i++){
                int a = main.getId(i);
                if(a == b){
                    int cla = main.getClas(i);
                    String matka = engine.executeScript("document.createPath("+ main.getArray(i) + ", 'red', '"+ cla +"')").toString();
                    main.addToLog(main.getTime() + " " + main.getName(i) + " lähetetty matkaan");
                    if ((cla == 1) || (cla == 3)){
                        if (main.getB(i) == true){
                            main.addToLog(main.getTime() + " " + main.getName(i) + " särkyi matkalla");
                        }
                    }
                    main.removePackage(i);
                    storage.getItems().clear();
                    for (int j = 0; j < main.getSize(); j++){
                        storage.getItems().addAll(main.getId(j) + "\t" + main.getName(j));
                    }                   
                } 
            }         
        }  
    }
    
    /**
     * Removes paths from map
     * 
     * @param event 
     */
    @FXML
    private void handleRemovePathsAction(ActionEvent event) {
        engine.executeScript("document.deletePaths()");
        main.addToLog(main.getTime() + " Reitit pyyhitty pois kartalta");
    }

    /** Opens log window
     * 
     * @param event
     * @throws IOException 
     */
    @FXML
    private void handleOpenLogAction(ActionEvent event) throws IOException {
        main.openLog();   
    }
    
}
