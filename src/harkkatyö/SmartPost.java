package harkkatyö;

/**
 * Author: Teemu Tynkkynen
 * Student number: 0418815
 * Date: 15.12.2015
 * 
 * Creates smartpost object
 */
public class SmartPost {
    
    private String code;
    private String city;
    private String address;
    private String availability;
    private String postOffice;
    private GeoPoint geopoint;
    
    /**
     * 
     * @param co
     * @param ci
     * @param ad
     * @param avai
     * @param postof
     * @param lat
     * @param lng 
     */
    public SmartPost(String co,String ci, String ad, String avai, String postof, String lat, String lng) {
        geopoint = new GeoPoint(lat,lng);
        code = co;
        city = ci;
        address = ad;
        availability = avai;
        postOffice = postof;
    }
    
    /**
     * Gives postcode of smartpost
     * 
     * @return 
     */
    public String getCode(){
        return code;
    }
    
    /**
     * Gives city of smartpost
     * 
     * @return 
     */
    public String getCity(){
        return city;
    }
    
    /**
     * Gives address of smartpost
     * 
     * @return 
     */
    public String getAddress(){
        return address;
    }
    
    /**
     * Gives availability of smartpost
     * 
     * @return 
     */
    public String getAvailability(){
        return availability;
    }
    
    /**
     * Gives postoffice of smartpost
     * 
     * @return 
     */
    public String getPostOffice(){
        return postOffice;
    }
    
    /**
     * Gives latiude of smartpost
     * 
     * @return 
     */
    public String getLat(){
        return geopoint.getLat();
    }
    
    /**
     * Gives longitude of smartpost
     * 
     * @return 
     */
    public String getLng(){
        return geopoint.getLng();
    }
  
}
