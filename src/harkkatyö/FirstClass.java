package harkkatyö;

/**
 * Author: Teemu Tynkkynen
 * Student number: 0418815
 * Date: 17.12.2015
 *
 * First class package
 */
public class FirstClass extends Package{
    
    public FirstClass(String a, String b, double c, boolean d){
        super(a,b,c,d,1); 
    }
    

}
