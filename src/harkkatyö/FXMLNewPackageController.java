package harkkatyö;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.Image;
import javafx.stage.Stage;

/**
 * Author: Teemu Tynkkynen
 * Student number: 0418815
 * Date: 16.12.2015
 *
 * Fxml window which user can make new package
 */
public class FXMLNewPackageController implements Initializable{

    Main main = Main.getInstance();
    ToggleGroup group = new ToggleGroup(); //Togglegroup for radio buttons
    
    @FXML
    private ComboBox<String> itemsCombo;
    @FXML
    private TextField weight;
    @FXML
    private TextField name;
    @FXML
    private TextField size;
    @FXML
    private CheckBox checkbox;
    @FXML
    private RadioButton first;
    @FXML
    private RadioButton second;
    @FXML
    private RadioButton third;
    @FXML
    private Button infoButton;
    @FXML
    private ComboBox<String> startCity;
    @FXML
    private ComboBox<String> startAuto;
    @FXML
    private Button createPackage;
    @FXML
    private Button cancelButton;
    @FXML
    private ComboBox<String> targetCity;
    @FXML
    private ComboBox<String> targetAuto;
    @FXML
    private Button clearItem;

    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        first.setToggleGroup(group);
        first.setUserData(1);
        second.setToggleGroup(group);
        second.setUserData(2);
        third.setToggleGroup(group);
        third.setUserData(3);
        group.selectToggle(first);

        // Add start cities to combobox
        for (int i = 0; i < main.getLenght(); i++){
            String city = main.getCity(i);
            if (i == 0) {
                startCity.getItems().addAll(city);
            } else {
                if (city.equals(main.getCity(i -1))) {
                } else {
                    startCity.getItems().addAll(city);
                }
            }
        }
        // Add target cities to combobox
        for (int j = 0; j < main.getLenght(); j++){
            String city = main.getCity(j);
            if (j == 0) {
                targetCity.getItems().addAll(city);
            } else {
                if (city.equals(main.getCity(j -1))) {
                } else {
                    targetCity.getItems().addAll(city);
                }
            }
        }
        // Add items to combobox
        for (int k = 0; k < 6; k ++) {
            itemsCombo.getItems().addAll(main.getNameFromList2(k));
        }
    }    
    
    /**
     * Opens info window about package classes
     * 
     * param event
     * @throws IOException 
     */
    @FXML
    private void handleInfoButtonAction(ActionEvent event) throws IOException {
        Stage stage = new Stage();
        Parent page = FXMLLoader.load(getClass().getResource("FXMLInfo.fxml"));
        Scene scene = new Scene(page);
        stage.setScene(scene);
        stage.getIcons().add(new Image(getClass().getResourceAsStream("res/T-logo.png")));
        stage.setTitle("Infoa pakettiluokista");
        stage.show();
    }
    
    /**
     * Fullfills startAuto combobox with automatons when user selects starting city
     * 
     * @param event 
     */
    @FXML
    private void handleStartCityAction(ActionEvent event) {
        startAuto.getItems().clear();
        String city = startCity.getValue();
        for (int i = 0; i < main.getLenght(); i++) {
            if (city.equals(main.getCity(i))){
                startAuto.getItems().addAll(main.getPostOffice(i).substring(19));
            }
        }
    }

    /**
     * Creates new package from given informations
     * 
     * @param event
     * @throws IOException 
     */
    @FXML
    private void handleCreatePackageAction(ActionEvent event) throws IOException {
        //Saves packages class
        String clas = group.getSelectedToggle().getUserData().toString();
        int cla = Integer.parseInt(clas);
        boolean b = checkbox.isSelected();
        // Checks if items combobox is empty
        if (itemsCombo.getValue() != null) {
            // Checks if starting location or target location is empty
            if ((startAuto.getValue() == null) || (targetAuto.getValue() == null)){
                main.setError("Anna lähtö- sekä kohdeautomaatti");
                main.createErrorWindow();
            } else {
                String slat = main.getLat("Pakettiautomaatti, " + startAuto.getValue());
                String slng = main.getLng("Pakettiautomaatti, " +startAuto.getValue());
                String tlat = main.getLat("Pakettiautomaatti, " +targetAuto.getValue());
                String tlng = main.getLng("Pakettiautomaatti, " +targetAuto.getValue());
                // If package are first class, makes first class package
                if (itemsCombo.getValue().equals("Kahvipaketti") || itemsCombo.getValue().equals("Pitsa")){
                    double dist = main.makeDistance(main.makeArray(slat, slng, tlat, tlng));
                    // Checks if distance if smaller than 150 km
                    if (dist <= 150){
                        main.handleItem(itemsCombo.getValue(), main.makeArray(slat, slng, tlat, tlng), 1, startCity.getValue(), targetCity.getValue());
                        main.addToLog(main.getTime() + " " + itemsCombo.getValue() + " esineestä luotu uusi paketti");
                        main.addToLog(main.getTime() + " Uusi paketti lisätty varastoon");
                        Stage stage = (Stage)cancelButton.getScene().getWindow();
                        stage.close();
                    // Gives error if distance is over 150 km
                    } else {
                        main.setError("Liian pitkä matka 1. luokalle");
                        main.createErrorWindow();
                    }
                }
                // If package are second class, makes second class package
                else if (itemsCombo.getValue().equals("Kalle") || itemsCombo.getValue().equals("Tietokone")){
                    main.handleItem(itemsCombo.getValue(), main.makeArray(slat, slng, tlat, tlng), 2,startCity.getValue(), targetCity.getValue());
                    main.addToLog(main.getTime() + " " + itemsCombo.getValue() + " esineestä luotu uusi paketti");
                    main.addToLog(main.getTime() + " Uusi paketti lisätty varastoon");
                    Stage stage = (Stage)cancelButton.getScene().getWindow();
                    stage.close();
                }
                // If package are third class, makes third class package
                else if (itemsCombo.getValue().equals("Betonimöhkäle") || itemsCombo.getValue().equals("Renkaat")){
                    main.handleItem(itemsCombo.getValue(), main.makeArray(slat, slng, tlat, tlng), 3, startCity.getValue(), targetCity.getValue());
                    main.addToLog(main.getTime() + " " + itemsCombo.getValue() + " esineestä luotu uusi paketti");
                    main.addToLog(main.getTime() + " Uusi paketti lisätty varastoon");
                    Stage stage = (Stage)cancelButton.getScene().getWindow();
                    stage.close();
                }
            }
        }
        // Checks if user hasn't choose start and target automatons
        else if (name.getText().equals("") || size.getText().equals("") || weight.getText().equals("")){
            main.setError("Täytä kaikki kohdat");
            main.createErrorWindow();
        // Make new package from users informations   
        } else {
            // Checks if starting location or target location is empty
            if ((startAuto.getValue() == null) || (targetAuto.getValue() == null)){
                main.setError("Anna lähtö- sekä kohdeautomaatti");
                main.createErrorWindow();
            } else {
                String slat = main.getLat("Pakettiautomaatti, " + startAuto.getValue());
                String slng = main.getLng("Pakettiautomaatti, " +startAuto.getValue());
                String tlat = main.getLat("Pakettiautomaatti, " +targetAuto.getValue());
                String tlng = main.getLng("Pakettiautomaatti, " +targetAuto.getValue());
                double dist = main.makeDistance(main.makeArray(slat, slng, tlat, tlng));
                String size3 = size.getText().replace(" ", "");
                size3 = size3.replace(".", "");
                String[] size2 = size3.split("x");
                if (size2.length == 3){
                    boolean value = main.checkNumber(size2, 0); // Checks if user has filled size correctly
                    if (value == true){
                        int[] ii = new int[3];
                        int j = Integer.parseInt(size2[0]);
                        ii[0] = j;
                        int k = Integer.parseInt(size2[1]);
                        ii[1] = k;
                        int l = Integer.parseInt(size2[2]);
                        ii[2] = l;
                        String[] wei = new String[1];
                        wei[0] = weight.getText();
                        if (main.checkNumber(wei, 1) == true){ // Checks if user has filled weight correctly
                            Double w = Double.parseDouble(wei[0]);
                            // Create first class package
                            if (cla == 1){
                                if (dist > 150){
                                    main.setError("Liian pitkä matka 1. luokalle");
                                    main.createErrorWindow();
                                } else {
                                    main.createPackage(name.getText(), size.getText(), w, b, cla, main.makeArray(slat, slng, tlat, tlng), main.makeDistance(main.makeArray(slat, slng, tlat, tlng)), startCity.getValue(), targetCity.getValue() );
                                    main.addToLog(main.getTime() + " " + name.getText() + " esineestä luotu uusi paketti");
                                    main.addToLog(main.getTime() + " Uusi paketti lisätty varastoon");
                                    Stage stage = (Stage)cancelButton.getScene().getWindow();
                                    stage.close();
                                }
                                
                            }
                            // Creates second class package
                            if (cla == 2 ) {
                                // Checks if items is too big
                                if ((j > 50) || (k > 50) || (l > 50)){
                                    main.setError("Paketti on liian suuri luokalle");
                                    main.createErrorWindow();
                                } 
                                // Checks if item is too heavy
                                else if (w > 15){
                                    main.setError("Paketti on liian painava luokalle");
                                    main.createErrorWindow();
                                } else {
                                    main.createPackage(name.getText(), size.getText(), w, b, cla, main.makeArray(slat, slng, tlat, tlng), main.makeDistance(main.makeArray(slat, slng, tlat, tlng)), startCity.getValue(), targetCity.getValue());
                                    main.addToLog(main.getTime() + " " + name.getText() + " esineestä luotu uusi paketti");
                                    main.addToLog(main.getTime() + " Uusi paketti lisätty varastoon");
                                    Stage stage = (Stage)cancelButton.getScene().getWindow();
                                    stage.close();
                                }   
                            }
                            // Creates third class package
                            if (cla == 3) {
                                main.createPackage(name.getText(), size.getText(), w, b, cla, main.makeArray(slat, slng, tlat, tlng), main.makeDistance(main.makeArray(slat, slng, tlat, tlng)), startCity.getValue(), targetCity.getValue());
                                main.addToLog(main.getTime() + " " + name.getText() + " esineestä luotu uusi paketti");
                                main.addToLog(main.getTime() + " Uusi paketti lisätty varastoon");
                                Stage stage = (Stage)cancelButton.getScene().getWindow();
                                stage.close();
                            }
                        }
       
                    }
                // Gives error if user hasn't filled size correctly
                } else {
                    main.setError("Koko-kenttä virheellinen");
                    main.createErrorWindow();
                } 
            } 
        } 
    }
    
    /**
     * Cancel package creation and close window
     * 
     * @param event 
     */
    @FXML
    private void handleCancelButtonAction(ActionEvent event) {
        Stage stage = (Stage)cancelButton.getScene().getWindow();
        stage.close();
    }
    
    /**
     * Fullfills targettAuto combobox with automatons when user selects starting city
     * 
     * @param event 
     */
    @FXML
    private void handleTargetCityAction(ActionEvent event) {
        targetAuto.getItems().clear();
        String city = targetCity.getValue();
        for (int i = 0; i < main.getLenght(); i++) {
            if (city.equals(main.getCity(i))){
                targetAuto.getItems().addAll(main.getPostOffice(i).substring(19));
            }
        }
    }

    /**
     * Clears itemsCombo combobox
     * 
     * @param event 
     */
    @FXML
    private void handleClearItemAction(ActionEvent event) {
        itemsCombo.setValue(null);
        
    }
        
}
