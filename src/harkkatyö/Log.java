package harkkatyö;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Author: Teemu Tynkkynen
 * Student number: 0418815
 * Date: 16.12.2015
 *
 * Class which makes log textfile
 */
public class Log {
    private ArrayList<String> array = new ArrayList<>();
    
    public Log(){
        
    }
    
    /**
     * Adds line to log
     * 
     * @param line 
     */
    public void addToLog(String line){
        array.add(line);
    }
    
    /**
     * Gives line from log
     * 
     * @param i
     * @return 
     */
    public String getFromLogArray(int i){
        return array.get(i);
    }
    
    /**
     * Gives lenght of array
     * 
     * @return 
     */
    public int getArrayLenght(){
        return array.size();
    }
    
    /**
     * Makes log file
     * 
     * @throws FileNotFoundException
     * @throws IOException 
     */
    public void makeLogFile() throws FileNotFoundException, IOException{
        try (BufferedWriter out = new BufferedWriter(new FileWriter("log.txt"))) {
            for (String array1 : array) {
                out.write(array1 + ";");
                out.newLine();
            }
        }
    }
    
    /**
     * Reads log file and adds it to log
     * 
     * @param time
     * @throws FileNotFoundException
     * @throws IOException 
     */
    public void readLog(String time) throws FileNotFoundException, IOException{
        File f = new File("log.txt");
        // Checks if file don't exist
        if (!f.exists()){
            
        } else {
            try (BufferedReader in = new BufferedReader(new FileReader("log.txt"))) {
                String line;
                String line2[];
                while ((line = in.readLine()) != null) {
                    line2 = line.split(";");
                    for (String line21 : line2) {
                        array.add(line21 + "\n");
                    }      
                }
            }
            array.add(time  + " Ohjelma kännistetty\n");
        }
    }  
}
