package harkkatyö;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.stage.Stage;

/** Author: Teemu Tynkkynen
 *  Studentnumber: 0418815
 *  Date: 15.12.2015
 * 
 * FXML controller for log window
 */

public class FXMLLogWindowController implements Initializable {
    @FXML
    private TextArea storage;
    @FXML
    private TextArea log;
    @FXML
    private Button clear;
    @FXML
    private Button close;
    @FXML
    private Button refreshLog;
    
    Main main = Main.getInstance();

    /**
     * Initialize controller class
     *
     * @param url
     * @param rb 
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        storage.clear();
        storage.appendText("Paketteja varastossa " + main.getLenght2() + " kappaletta.\n\n");
        // Updates storage
        for (int i = 0; i < main.getLenght2(); i++) {
            int id = main.getId(i);
            String name = main.getName(i);
            double dist = main.getDistance(i);
            int clas = main.getClas(i);
            String from = main.getFrom(i);
            String to = main.getTo(i);
            storage.appendText("\t"+id+" "+name+ " " + from + "-" + to + " " + dist + "km\n" );
        }
        log.clear();
        //Updates log
        for (int i = 0; i < main.getLogLenght(); i++){
            log.appendText(main.getFromLog(i));
        }
    }    
    
    /**
     * Closes window
     * 
     * @param event 
     */
    @FXML
    private void handleCloseButton(ActionEvent event) {
        Stage stage = (Stage)close.getScene().getWindow();
        stage.close();
    }
    
    /** 
     * Refresh storage and log
     * 
     * @param event 
     */
    @FXML
    private void handleRefreshLogAction(ActionEvent event) {
        // Refresh log
        log.clear();
        for (int i = 0; i < main.getLogLenght(); i++){
            log.appendText(main.getFromLog(i));
        }
        //Refresh storage
        storage.clear();
        storage.appendText("Paketteja varastossa " + main.getLenght2() + " kappaletta.\n\n");
        for (int i = 0; i < main.getLenght2(); i++) {
            int id = main.getId(i);
            String name = main.getName(i);
            double dist = main.getDistance(i);
            String from = main.getFrom(i);
            String to = main.getTo(i);
            storage.appendText("\t"+id+" "+name+ " " + from + "-" + to + " " + dist + "km\n" );
        }
    }
}
